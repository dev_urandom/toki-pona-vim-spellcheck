# toki pona Vim spellcheck

This is a set of files that can be used for spellchecking toki pona texts in Vim.

## Installation

1. Create a `~/.vim/spell/` directory, if it doesn't exist.

2. Open Vim, and launch the following command inside it:

`:mkspell tp.utf-8.spl tokipona`

3. Copy the `tp.vim` and `tp.utf-8.spl` files into the `~/.vim/spell/`
directory.

## Usage

`:set spell` to enable spellchecking.

`:set spelllang=tp` to set spellcheck to toki pona.

All official words (including both ale and ali) are recognized. Some additional
words are specified as "rare words", which means they'll be highlighted, but not
in the same way as outright errors. 

Any unofficial words that follow the toki pona syllable structure will not be
marked as errors, either.

You can modify the dictionary yourself by editing the `tokipona.dic` file, but
you'll need to recompile the `tp.utf-8.spl` file by running the `:mkspell`
command afterwards.

# lipu pi pona sitelen pi toki pona tawa ilo Vim

lipu ni li pali tawa ni: ilo Vim li sona e ni: nimi seme li pona tawa toki pona?
ilo Vim li ken kule e nimi ike tawa ni: sina pona e ona.

## o kama jo e lipu ni

1. sina jo ala e poki `.vim/spell/` lon poki tomo sina, o pali e ona.

2. o open e ilo Vim, o pana e nimi pali ni:

`:mkspell tp.utf-8.spl tokipona`

3. o pana sin e lipu `tp.vim`, e lipu `tp.utf-8.spl` tawa poki `.vim/spell` pi
poki tomo sina.

## o kepeken e lipu ni

o pana e nimi pali `:set spell` tawa ni: ilo Vim li lukin e nimi ike.

o pana e nimi pali `:set spelllang=tp` tawa ni: ilo Vim li sona e ni: sina toki
kepeken toki pona.

nimi ale pu li nimi pona. sin la, nimi sin mute li pona. nimi sin li kule
kepeken nasin ni: "jan li toki e nimi ni lon tenpo pi mute lili".
  
sin la, nimi pi sitelen suli ale li pona tawa nasin kalama pi toki pona la, ona li pona tawa lipu ni.

sina ken ante a lipu pi nimi pona kepeken lipu `tokipona.dic`. sina pali e
ante la, o pali e lipu `tp.utf-8.spl` sin kepeken nimi pali `:mkspell`.
